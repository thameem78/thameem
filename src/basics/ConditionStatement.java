package basics;

import java.util.Scanner;

public class ConditionStatement {

    public static void main(String[] args) {
        System.out.println("Condition Statement");
        vote();
        greatNumber();
    }

    static void vote() {
        System.out.println("Voting");
        Scanner in = new Scanner(System.in);

        int age = in.nextInt();
        if (age >= 18) {
            System.out.println("Eligible to vote");
        } else {
            System.out.println("Not Eligible");
        }
    }

    static void greatNumber() {
        //Take the 3 numbers from the user & print the greatest number
        System.out.println("Greatest Number");
        Scanner in = new Scanner(System.in);

        System.out.print("Input the 1st number: ");
        int num1 = in.nextInt();

        System.out.print("Input the 2nd number: ");
        int num2 = in.nextInt();

        System.out.print("Input the 3rd number: ");
        int num3 = in.nextInt();

        if (num1 > num2) {
            if (num1 > num3) {
                System.out.println("The greatest:" + num1);
            }
        }
        if (num2 > num1) {
            if (num2 > num3) {
                System.out.println("The greatest:" + num2);
            }
        }
        if (num3 > num1) {
            if (num3 > num2) {
                System.out.println("The greatest:" + num3);
            }
        }
    }
}
