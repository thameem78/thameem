package basics;

public class Operators {
  
    public static void main(String args[]){
        System.out.println("Operators");   
      arithmetic();
      assignment();
      comparison();
      logic();      
    }
    
    static void arithmetic(){
        //Arithmetic Operator(+,-,*,/,%)
       int a=10;
       int b=20;
       
       System.out.println("Arithmetic Operators");
       System.out.println(a+b);
       System.out.println(a-b);
       System.out.println(a*b);
       System.out.println(a/b);
       System.out.println(a%b);
}
    
    static void assignment(){
    //Assignment Operator(=,+=,-=,*=,/=,%=,&=,|=)
       int x=5;
       System.out.println("Assignment Operator"
       );
       System.out.println(x);
       
       x+=5;
       System.out.println(x);

       x-=5;
       System.out.println(x);

       x*=5;
       System.out.println(x);

       x/=5;
       System.out.println(x);

       x%=5;
       System.out.println(x);
}
    
    static void comparison(){
    //comparison operator
      
       int c=1;
       int d=1;
       
       System.out.println("Comparison Operator");
       System.out.println(c==d);
       System.out.println(c!=d);
       System.out.println(c>d);
       System.out.println(c<d);
       System.out.println(c>=d);
       System.out.println(c<=d);

}
    
    static void logic(){
    //Logical Operator
       int r=2;
       System.out.println("Logical Operator");
       System.out.println(r>3 && r<10);
       System.out.println(r>3 || r<10);
       System.out.println(!(r>3 && r<10));
    }
}





/*
Java Operator Precedence and Associativity
Operators                                        Precedence          Associativity
postfix increment and decrement                   ++ --               left to right
prefix increment and decrement, and unary         ++ -- + - ~ !       right to left
multiplicative                                    * / %               left to right
additive                                          + -                 left to right
shift                                             << >> >>>           left to right
relational                                       < > <= >= instanceof left to right
equality                                           == !=              left to right
bitwise AND                                        &                  left to right
bitwise exclusive OR                               ^                  left to right
bitwise inclusive OR                               |                  left to right
logical AND                                        &&                 left to right
logical OR                                         ||                 left to right
ternary                                            ? :                right to left
assignment                                      = += -= *= /= %=      right to left
                                                &= ^= |= <<= >>= >>>=	
*/




//(),/,*,+,-
//(4+5/2)=9/2