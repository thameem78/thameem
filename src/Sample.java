public class Sample {

    public static void main(String args[]) {
        System.out.println("Welcome");

        //Byte(-128 to 127)
        byte v = 100;
        System.out.println(v);
//Vanakkam
        //Short(-32768 to 32767)
        short c;
        c = 5000;
        System.out.println(c);

        //int (-2147483648 to 2147483647)
        int a;
        a = 10000;
        System.out.println(a);

        // long (-9223372036854775808 to 9223372036854775807)
        long myNum = 15000000000L;
        System.out.println(myNum);

        //float
        float b;
        b = 6.5f;
        System.out.println(b);

        //double
        double z = 19.99d;
        System.out.println(z);

        //char
        char i='M';
        System.out.println(i);
        
        //boolean
        boolean r = true;
        boolean p = false;
        System.out.println(r);
        System.out.println(p);

        //String
        String u;
        u = "Thameem";
        System.out.println(u);
    }
}